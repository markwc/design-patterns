#include <StateFunction.h>
#include <gtest/gtest.h>

using namespace Katas;

namespace
{

class Boss {
public:
  Boss() {
    srand(time(NULL));
  };

  ~Boss() = default;
  Boss(const Boss&) = delete;
  Boss(Boss&&) = delete;
  Boss& operator=(const Boss&) = delete;
  Boss& operator=(Boss&&) = delete;

  StateFunction state1() {
    std::cout << __func__ << std::endl;
    if (testN(2U)) {
      return std::bind(&Boss::state2, this);
    }
    return std::move(SF);
  }

  StateFunction state2() {
    std::cout << __func__ << std::endl;
    if (testN(4U)) {
      return std::bind(&Boss::state3, this);
    }
    return std::move(SF);
  }

  StateFunction state3() {
    std::cout << __func__ << std::endl;
    if (testN(8U)) {
      keep_going = false;
      return std::bind(&Boss::state1, this);
    }
    return std::move(SF);
  }

  bool update() {
    SF = SF();
    return keep_going;
  }

  static bool testN(uint32_t N) {
    return (rand() % N) != 0;
  }

private:
  StateFunction SF{std::bind(&Boss::state1, this)};
  bool keep_going{true};
};

class StateFunction_test : public ::testing::Test
{
public:
  uint32_t m_state_bits{0};

  StateFunction_test() = default;

  ~StateFunction_test() override = default;

  StateFunction_test(StateFunction_test &&) = delete;
  StateFunction_test(const StateFunction_test &) = delete;
  StateFunction_test &operator=(StateFunction_test &&) = delete;
  StateFunction_test &operator=(const StateFunction_test &) = delete;

protected:
  void SetUp() override
  {
  }

  void TearDown() override
  {
  }
};

TEST_F(StateFunction_test, Boss)
{
  Boss B;
  while (B.update()) {
  }
}

} // namespace