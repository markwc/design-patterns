#include <State.h>
#include <gtest/gtest.h>

#include <array>
#include <iostream>
#include <random>

namespace
{

class State_test : public ::testing::Test
{
public:
  uint32_t m_state_bits{0};

  State_test() = default;

  ~State_test() override = default;

  static void state_change_callback(void *context,
                                    Katas::state_return_code_t return_code)
  {
    if (nullptr != context)
    {
      State_test *p_this = reinterpret_cast<State_test*>(context);
      p_this->m_state_bits |= 0x1 << static_cast<uint32_t>(return_code);
    }
  }

  State_test(State_test &&) = delete;
  State_test(const State_test &) = delete;
  State_test &operator=(State_test &&) = delete;
  State_test &operator=(const State_test &) = delete;

protected:
  void SetUp() override
  {
  }

  void TearDown() override
  {
  }
};

TEST_F(State_test, get_instance)
{
  Katas::State UUT;
  UUT.register_state_change_callback(state_change_callback,
                                     static_cast<void *>(this));
  UUT.start();
  EXPECT_EQ(m_state_bits, 0x1E);
}

} // namespace