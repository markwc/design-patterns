#include <Singleton.h>
#include <gtest/gtest.h>

#include <array>
#include <random>

namespace
{

class Singleton_test : public ::testing::Test
{
public:
  Singleton_test() = default;

  ~Singleton_test() override = default;

  Singleton_test(Singleton_test &&) = delete;
  Singleton_test(const Singleton_test &) = delete;
  Singleton_test &operator=(Singleton_test &&) = delete;
  Singleton_test &operator=(const Singleton_test &) = delete;

protected:
  void SetUp() override
  {
  }

  void TearDown() override
  {
  }
};

TEST_F(Singleton_test, get_instance)
{
  auto UUT1 = Katas::Singleton::instance();
  auto UUT2 = Katas::Singleton::instance();
  ASSERT_EQ(UUT1, UUT2);
  ASSERT_EQ(UUT1->get_count(), UUT2->get_count());
  ASSERT_EQ(UUT1.use_count(), UUT2.use_count());
}

} // namespace