// StateFunction.h
// See: https://silverweed.github.io/Functional_State_Machines_in_C++/

#pragma once

#include <functional> // for std::function
#include <utility>    // for std::move

namespace Katas {
struct StateFunction {
  template <typename T> StateFunction(const T &f) : f(f) {}

  StateFunction(StateFunction &&s) = default;
  StateFunction(const StateFunction &s) = delete;

  StateFunction operator()() const { return f(); }

  StateFunction &operator=(StateFunction &&s) {
    f = std::move(s.f);
    return *this;
  }

  StateFunction &operator=(const StateFunction &) = delete;

  std::function<StateFunction()> f;
};
} // namespace Katas