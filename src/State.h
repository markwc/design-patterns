/// @file State.h

#ifndef SRC_STATE_H
#define SRC_STATE_H

#include <memory>

namespace Katas
{

enum class state_return_code_t : uint32_t
{
  code_1 = 1,
  code_2,
  code_3,
  code_4
};

using state_change_callback_t = void (*)(void* context, state_return_code_t return_code);
class State
{
public:
  State(State &&) = delete;
  State(const State &) = delete;
  State &operator=(State &&) = delete;
  State &operator=(const State &) = delete;

  State();

  ~State();

  void register_state_change_callback(state_change_callback_t callback, void* context);

  void start();

private:
  struct Impl;

  std::unique_ptr<Impl> m_p_impl;
};

} // namespace Katas

#endif // #define SRC_STATE_H
