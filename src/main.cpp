/// @file main.cpp

#include <Singleton.h>

#include <iostream>

int main() {
  std::cout << "Main run" << std::endl;
  return 0;
}