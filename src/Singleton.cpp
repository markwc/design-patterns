/// @file Singleton.cpp

#include <Singleton.h>

namespace Katas
{

struct Singleton::Impl
{
  uint32_t count{0};
};

Singleton::Singleton() : m_p_impl(std::make_unique<Impl>())
{
  m_p_impl->count++;
}

Singleton::~Singleton() = default;

std::shared_ptr<Singleton> Singleton::instance()
{
  static std::shared_ptr<Singleton> p_singleton(new Singleton());
  return p_singleton;
}

uint32_t Singleton::get_count()
{
  return m_p_impl->count;
}

} // namespace Katas
