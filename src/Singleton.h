/// @file Singleton.h

#ifndef SRC_SINGLETON_H
#define SRC_SINGLETON_H

#include <memory>

namespace Katas
{

class Singleton
{
public:
  Singleton(Singleton &&) = delete;
  Singleton(const Singleton &) = delete;
  Singleton &operator=(Singleton &&) = delete;
  Singleton &operator=(const Singleton &) = delete;
  static std::shared_ptr<Singleton> instance();
  uint32_t get_count();
  ~Singleton();

protected:
  Singleton();

private:
  struct Impl;
  std::unique_ptr<Impl> m_p_impl;
};
} // namespace Katas

#endif // #define SRC_SINGLETON_H
