/// @file State.cpp

#include <State.h>

#include <array>
#include <iostream>

namespace Katas
{

using state_function_t = state_return_code_t (*)();

struct state_transition_t
{
  state_function_t state_function;
  state_return_code_t return_code;
  state_function_t next_state;
};

static bool keep_going{true};

struct State::Impl
{
  static std::array<state_transition_t, 4> m_state_transition_table;
  state_change_callback_t m_state_change_callback{nullptr};
  void *m_state_change_callback_context{nullptr};

  Impl(){};

  void run()
  {
    state_return_code_t state_return_code = state_return_code_t::code_1;
    state_function_t state_function =
        m_state_transition_table[0].state_function;
    while (keep_going)
    {
      state_return_code = state_return_code_t::code_1;
      if (nullptr != state_function)
      {
        state_return_code = state_function();
        if (nullptr != m_state_change_callback)
        {
          m_state_change_callback(m_state_change_callback_context,
                                  state_return_code);
        }
        state_function = look_up_next_state(state_function, state_return_code);
      }
    }
  }

  ~Impl() = default;

  static state_return_code_t state_1()
  {
    return state_return_code_t::code_2;
  }

  static state_return_code_t state_2()
  {
    return state_return_code_t::code_3;
  }

  static state_return_code_t state_3()
  {
    return state_return_code_t::code_4;
  }

  static state_return_code_t state_4()
  {
    keep_going = false;
    return state_return_code_t::code_1;
  }

  state_function_t look_up_next_state(state_function_t state_function,
                                      state_return_code_t return_code)
  {
    state_function_t next_state_function = nullptr;
    for (auto &&state_transition : m_state_transition_table)
    {
      if ((reinterpret_cast<void *>(state_function) ==
           reinterpret_cast<void *>(state_transition.state_function)) &&
          (return_code == state_transition.return_code))
      {
        next_state_function = state_transition.next_state;
      }
    }
    return next_state_function;
  }

  Impl(Impl &&) = delete;
  Impl(const Impl &) = delete;
  Impl &operator=(Impl &&) = delete;
  Impl &operator=(const Impl &) = delete;
};

std::array<state_transition_t, 4> State::Impl::m_state_transition_table = {
    {{State::Impl::state_1, state_return_code_t::code_2, State::Impl::state_2},
     {State::Impl::state_2, state_return_code_t::code_3, State::Impl::state_3},
     {State::Impl::state_3, state_return_code_t::code_4, State::Impl::state_4},
     {State::Impl::state_4, state_return_code_t::code_4,
      State::Impl::state_1}}};

//-----------------------------------------------------------------------------

State::State() : m_p_impl(std::make_unique<Impl>())
{
}

State::~State() = default;

void State::register_state_change_callback(state_change_callback_t callback,
                                           void *context)
{
  m_p_impl->m_state_change_callback = callback;
  m_p_impl->m_state_change_callback_context = context;
}

void State::start()
{
  m_p_impl->run();
}

} // namespace Katas
